package solution.lesson04;

import org.junit.Test;

public class MaxCounters {

	@Test
	public void test() {
		int[] A = {3,4,4,6,1,4,4};
		int[] result = solution(5, A);
		for (int value : result) {
			System.out.print(value);	
		}
		
	}
	
	public int[] solution(int N, int[] A) {
		int[] result = new int[N];
		
		int fillCount = 0;
		for (int value : A) {
			if (value > N) {
				fillCount++;
				continue;
			}
			result[value-1]++;
		}
		
		for (int index=0 ; index < result.length ; index++) {
			result[index] = result[index] + fillCount;
		}
		
		return result;
	}
	
}
