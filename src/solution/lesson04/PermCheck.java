package solution.lesson04;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * TaskScore : 100%
 * Correctness : 100%
 * Performance : 100%
 * TimeSpent : 14min
 * Detected time complexity : O(N) or O(N * log(N))
 */
public class PermCheck {

    @Test
    public void runTest() {
        int[] A = {4, 1, 3};
        System.out.println(solution(A));
    }

    public int solution(int[] A) {

        Set<Integer> set = new HashSet<>();
        for (int value : A) {
            set.add(value);
            if (value < 1 || value > A.length) {
                return 0;
            }
        }
        if (set.size() != A.length) {
            return 0;
        }
        return 1;
    }
}
