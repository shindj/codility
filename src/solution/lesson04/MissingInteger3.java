package solution.lesson04;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

public class MissingInteger3 {

	@Test
	public void test() {
		int[] A = {1,3,6,4,1,2};
		System.out.println(solution(A));
	}
	
	public int solution(int[] A) {
		boolean[] result = new boolean[1000000];
		for(int value : A) {
			if (value <0) break;
			
			result[value-1] = true;
		}
		
		int index = 0;
		for(boolean b : result) {
			if (!b) {
				return index+1;
			}
			index++;
		}
		return 1;
		
	}
	
}
