package solution.lesson04;

import org.junit.Test;

/**
 * TaskScore : 100%
 * Correctness : 100%
 * Performance : 100%
 * TimeSpent : 30min
 * Detected time complexity : O(N) or O(N * log(N))
 */
public class MissingInteger2 {

	@Test
	public void test() {
		int[] A = {0};
		System.out.println(solution(A));
	}
	
	public int solution(int[] A) {
		boolean[] result = new boolean[1000000];
		for(int value : A) {
			if (value <= 0) continue;
			
			result[value-1] = true;
		}
		
		int index = 0;
		for(boolean b : result) {
			if (!b) {
				return index+1;
			}
			index++;
		}
		return 1;
		
	}
	
}
