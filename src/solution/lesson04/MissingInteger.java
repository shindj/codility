package solution.lesson04;

import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;

/**
 * TaskScore : 88%
 * Correctness : 100%
 * Performance : 75%
 * TimeSpent : 14min
 * Detected time complexity : O(N) or O(N * log(N))
 */
public class MissingInteger {

	@Test
	public void test() {
		int[] A = {1,3,6,4,1,2};
		System.out.println(solution(A));
	}
	
	public int solution(int[] A) {
		Set<Integer> set = new TreeSet<>();
		for (int value : A) {
			if(value < 0) {
				continue;
			}
			set.add(value);
		}
		
		int min = 0;
		
		for (int value : set) {
			if (min == value) {
				continue;
			}
			if (min+1 < value) {
				break;
			}
			if (min+1 == value) {
				min = value;
			}
			
		}
		if (min == 0) {
			return 1;
		} else {
			return min+1;	
		}
		
	}
	
}
