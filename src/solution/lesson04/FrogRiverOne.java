package solution.lesson04;

import org.junit.jupiter.api.Test;

/**
 * TaskScore : 100%
 * Correctness : 100%
 * Performance : 100%
 * TimeSpent : 27min
 * Detected time complexity : O(N)
 */
public class FrogRiverOne {

    @Test
    public void run() {
        int X = 3;
        int[] A = {1, 3, 1, 3, 2, 1, 3};
        System.out.println(solution(X, A));
    }

    public int solution(int X, int[] A) {
        return findFastestTime(X, A);
    }
    
    private int findFastestTime(int distance, int[] leaves) {
        int max = 0;
        for (Integer value : makeFrogPath(distance, leaves)) {
            if (value == null) {
                return -1;
            }
            max = max < value ? value : max;
        }

        return max;
    }

    private Integer[] makeFrogPath(int distance, int[] leaves) {
        Integer[] frogPath = new Integer[distance];

        int index = 0;
        for (int value : leaves) {
            boolean isFrogPath = value <= frogPath.length;
            boolean isNotDuplicated = frogPath[value - 1] == null;

            if (isFrogPath && isNotDuplicated) {
                frogPath[value - 1] = index;
            }

            index++;
        }
        return frogPath;
    }
}
