package solution.lesson01;

import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

public class BinaryGap_Mine {
	
	public int solution(int N) {
		String binaryStr = Integer.toBinaryString(N);
		System.out.println(N);
		System.out.println(binaryStr);
		if (binaryStr.length() <= 1) {
			return 0;
		}

		int count = 0;
		int max = 0;
		boolean firstCheck = false;
		boolean lastCheck = false;

		for (char cursorValue : binaryStr.toCharArray()) {
			if (cursorValue == '1' && firstCheck) {
				lastCheck = true;
			}
			if (cursorValue == '1' && !firstCheck) {
				firstCheck = true;

			}
			if (cursorValue == '0' && firstCheck) {
				count++;
			}
			if (firstCheck && lastCheck) {
				if (max < count) {
					max = count;
				}
				count = 0;
				firstCheck = true;
				lastCheck = false;
			}
		}
		System.out.println(max);
		System.out.println();
		return max;
	}

	@Test
	public void runTest() {
		IntStream.range(0, 6480).forEach(i -> solution(i));
	}
}
