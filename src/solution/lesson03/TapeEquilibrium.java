package solution.lesson03;

import org.junit.jupiter.api.Test;

public class TapeEquilibrium {
	
	/**
	 * TaskScore : 100%
	 * Correctness : 100%
	 * Performance : 100%
	 * TimeSpent : 14min
	 * Detected time complexity : O(N)
	 */

	@Test
	public void runTest() {
		int[] a = { 3, 1, 2, 4, 3 };
		int result = solution(a);
		System.out.println(result);
	}

	public int solution(int[] A) {
		int[] accumulatedSumArr = getAccumulatedSumArr(A);
		
		int min = 0;
		for (int P = 1; P < A.length; P++) {
			int firstPartSum = accumulatedSumArr[P - 1];
			int secondPartSum = accumulatedSumArr[accumulatedSumArr.length - 1] - firstPartSum;
			
			int absoulteDiffrence = firstPartSum - secondPartSum;
			absoulteDiffrence = absoulteDiffrence < 0 ? -absoulteDiffrence : absoulteDiffrence;
			
			min = P == 1 ? absoulteDiffrence : Integer.min(absoulteDiffrence, min);
		}

		return min;
	}
	
	private int[] getAccumulatedSumArr(int[] arr) {
		int[] accumulatedSumArr = new int[arr.length];
		for (int index = 0; index < arr.length; index++) {
			if (index == 0) {
				accumulatedSumArr[index] = arr[index];
			} else {
				accumulatedSumArr[index] = accumulatedSumArr[index - 1] + arr[index];
			}
		}
		return accumulatedSumArr;
	}
}
