package solution.lesson03;

import org.junit.jupiter.api.Test;

public class PermMissingElem_Mine1 {

	/**
	 * 1차시도
	 * 
	 * TimeSpect : 21min
	 * TaskScore : 50%
	 * Correctness : 100%
	 * Performance : 0%
	 */
	@Test
	public void runTest() {
		int[] data = {2,3,1,5}; // number 4 is expected.
		System.out.println(this.solution(data));
	}
	
	public int solution(int[] A) {
		float size = A.length; // 4
		float min = 1;
		float max = size+1;
		float middle = max / 2;
		
		float expectedSum = (min+max) * middle;
		
		float sum = 0;
		
		for (int cursorVal : A) {
			sum += cursorVal;
		}
		
		return (int) (expectedSum - sum);
	}
}
