package solution.lesson03;

import org.junit.jupiter.api.Test;

public class FrogJmp_Mine {
	
	/**
	 * TimeSpect : 4min
	 * TaskScore : 100%
	 * Correctness : 100%
	 * Performance : 100%
	 */
	@Test
	public void runTest() {
		System.out.println(solution(10, 85, 30));
	}
	
	public int solution(int X, int Y, int D) {
		int jumpDistance = D;
		int startPosition = X;
		int targetPosition = Y;
		
		int targetDistance = targetPosition - startPosition;
		int rest = targetDistance % jumpDistance;
		int count = targetDistance / jumpDistance;
		
		if (rest != 0) {
			count++;
		}
		
		return count;
	}

}
