package solution.lesson03;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class PermMissingElem_Mine2 {

	/**
	 * 2차시도
	 * 
	 * float연산에 오차가 있는 것 같아서, BigDecimal로 수정하였음.
	 * (N이 대략 5800을 넘어가면서부터 오차가 발생.)
	 * BigDecimal 생성에 따른 오버헤드가 걱정되었으나, 퍼포먼스가 100%로 출력됨.
	 * 
	 * TaskScore : 100%
	 * Correctness : 100%
	 * Performance : 100%
	 * 
	 * Detected time complexity: O(N) or O(N * log(N))
	 */
	@Test
	public void runTest() {
		int[] data = { 2, 3, 1, 5 }; // number 4 is expected.
		System.out.println(this.solution(data));
	}

	public int solution(int[] A) {
		BigDecimal size = new BigDecimal(A.length);
		BigDecimal min = new BigDecimal(1);
		BigDecimal max = size.add(min);
		BigDecimal middle = max.divide(new BigDecimal(2));

		BigDecimal expectedSum = min.add(max).multiply(middle);

		for (int cursorVal : A) {
			expectedSum = expectedSum.subtract(new BigDecimal(cursorVal));
		}

		return expectedSum.intValue();
	}
}
