package solution.lesson02;

import org.junit.jupiter.api.Test;

/**
 * 
 * XOR연산 -> bit연산을 통해 같으면0, 다르면1로 바꾸기 때문에. 같은값을 두번 연산하면 원래값이 된다.
 * 
 * step1 : 1001 ^ 0001 = 1000
 * step2 : 1000 ^ 0001 = 1001 (원복)
 * 
 * step3 : 1001 ^ 0011 = 1010
 * step4 : 1010 ^ 0011 = 1001 (원복) 
 */
public class OddOccurrencesInArray_Solution {

	@Test
	public void runTest() {
		int[] A = { 9, 3, 9, 3, 9, 7, 9 };
		System.out.println(solution(A));
	}

	public int solution(int[] A) {

		int res = 0;
		for (int i = 0; i < A.length; i++) {
			res ^= A[i];
		}

		return res;
	}

}