package solution.lesson02;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * 
 * 첫번째 시도 : 30m 소요, 문제 잘못이해함 correctNess 40%
 * 두번째 시도 : 30m 소요, correctness 100%, 퍼포먼스 테스트timeout발생
 *
 */
public class OddOccurrencesInArray_Mine {

	@Test
	public void runTest() {
		int[] A = { 9, 3, 9, 3, 9, 7, 9 };
		System.out.println(solution(A));
	}

	public int solution(int[] A) {
		List<Integer> list = new LinkedList<Integer>();

		for (int a : A) {
			boolean result = list.removeIf(val -> val == a);
			if (!result) {
				list.add(a);
			}

		}
		return list.get(0);
	}

}
