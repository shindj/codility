package solution.lesson02;

import org.junit.jupiter.api.Test;

public class CyclicRotation_Mine {
	
	public int[] solution(int[] A, int K) {
		print(A);

		for (int i = 0; i < K; i++) {
			A = lotate(A);
			print(A);
		}

		return A;
	}

	public int[] lotate(int[] A) {
		int[] lotateArr = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			if (i == A.length - 1) {
				lotateArr[0] = A[i];
			} else {
				lotateArr[i + 1] = A[i];
			}
		}

		return lotateArr;
	}

	public void print(int[] A) {
		for (int a : A) {
			System.out.print(a);
		}
		System.out.println();
	}

	@Test
	public void runTest() {
		int[] a = { 1, 2, 3, 4, 5 };
		solution(a, 3);
	}
}
