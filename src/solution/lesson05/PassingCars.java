package solution.lesson05;

import org.junit.Test;

/**
 * TaskScore : 100%
 * Correctness : 100%
 * Performance : 100%
 * TimeSpent : 10min
 * Detected time complexity : O(N)
 */
public class PassingCars {

	@Test
	public void test() {
		int[] arr = { 0, 1, 0, 1, 1 };
		System.out.println(solution(arr));
	}
	
	public int solution(int[] A) {
		
		int zeroCount = 0;
		int pairCount = 0;
		for (int value : A) {
			if (value == 0) {
				zeroCount++;
			} else {
				pairCount += zeroCount;
			}
			
			if(pairCount > 1000000000)
				return -1;
		}
		
		return pairCount;
	}
	
}
