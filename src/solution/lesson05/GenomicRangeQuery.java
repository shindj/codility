package solution.lesson05;

import org.junit.Test;

/**
 * TaskScore : 62%
 * Correctness : 100%
 * Performance : 0%
 * TimeSpent : 38min
 * Detected time complexity : O(N * M)
 */
public class GenomicRangeQuery {

	@Test
	public void test() {
		int[] P = { 2, 5, 0 };
		int[] Q = { 4, 5, 6};
		String S = "CAGCCTA";
		for (int val : solution(S, P, Q)) {
			System.out.println(val);	
		}
		
	}
	
	public int[] solution(String S, int[] P, int[] Q) {
		int[] result = new int[P.length];
		
		
		char[] sChars = S.toCharArray();
		int[] convertedS = new int[S.length()];
		for(int index=0 ; index < S.length() ; index++) {
			convertedS[index] = convert(sChars[index]);
		}
		
		// O(N*M) 발생부분
		for(int index=0 ; index < P.length ; index++) {
			result[index] = search(convertedS, P[index], Q[index]);
		}
		
		return result;
	}
	
	private int search(int[] convertedS, int p, int q) {
	
		int minAffectedEl = -1;
		for (int index=p ; index <= q ; index++) {
			int cursorVal = convertedS[index];
			if (minAffectedEl < 0 || minAffectedEl > cursorVal) {
				minAffectedEl = cursorVal;
			}
			if (minAffectedEl == 1) {
				return 1;
			}
		}
		return minAffectedEl;
	}
	
	private int convert(char c) {
		switch(c) {
		case 'A':
			return 1;
		case 'C':
			return 2;
		case 'G':
			return 3;
		case 'T':
			return 4;
		default:
			return 0;
		}
	}
}
