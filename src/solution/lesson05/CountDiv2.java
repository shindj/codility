package solution.lesson05;

import org.junit.Test;
/**
 * TaskScore : 100%
 * Correctness : 100%
 * Performance : 100%
 * TimeSpent : 60min
 * Detected time complexity : O(1)
 */
public class CountDiv2 {

	@Test
	public void test() {
		System.out.println(solution(11, 345, 17));
		System.out.println(solution(6, 11, 2));
	}
	
	public int solution(int A, int B, int K) {
		int a = A < K ? 0 : A / K;
		return (B / K) - (A % K == 0 ? a-1 : a);
	}
}
